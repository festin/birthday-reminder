var debug = 0
function analyzePage(colName, colBDate) {
	var allLines = document.evaluate("//tr", document.documentElement, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
	var bDays = {}
	var message
	colName--;
	colBDate--;
	for(var i=0; i<allLines.snapshotLength; i++){
		item = allLines.snapshotItem(i)
		person = document.evaluate("td", item, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
		if (null == person || person.snapshotItem(colName) === null || person.snapshotItem(colBDate) === null) continue;
		person_name = person.snapshotItem(colName).innerText // validate
		person_birthdate = person.snapshotItem(colBDate).innerText //validate
		if (debug > 0) console.log(person_name + " -> " + person_birthdate)
		if (null == bDays[person_birthdate]) bDays[person_birthdate] = [];
		bDays[person_birthdate].push(person_name)
	}
	if (Object.keys(bDays).length !== 0) {
		message = {type: 'bDays', content: bDays, "colName": ++colName, "colBDate": ++colBDate}
	} else {
		message = {type: 'emptyBDays'}
	}
	chrome.extension.sendMessage(message)
}

chrome.runtime.onMessage.addListener(function(message, sender, response) {
	if (message.action == 'analyzePage') {
		analyzePage(message.colName, message.colBDate);
	}
});