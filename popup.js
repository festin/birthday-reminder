function onClickAnalyze() {
	var colName = document.getElementById('txtColumnName').value;
	var colBDate = document.getElementById('txtColumnNBirthdate').value;
	chrome.tabs.query({"active": true}, function(result) {
		chrome.tabs.sendMessage(result[0].id, {"action": "analyzePage", "colName": colName, "colBDate": colBDate});
	});	
}

document.addEventListener('DOMContentLoaded', function () {
	var btnAnalyze = document.getElementById('btnAnalyze');
	btnAnalyze.addEventListener('click', onClickAnalyze);
	document.getElementById('txtColumnName').value = localStorage.colName;
	document.getElementById('txtColumnNBirthdate').value = localStorage.colBDate;
});

