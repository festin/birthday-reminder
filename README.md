Project of Chrome extension to remind about birthdays in users list.

1. Install this extension.
2. Open the page with table of people birthdays (it must be HTML table).
3. Click "Analyze page" in extension menu (click on button on chrome bar).
4. See "BDays received!'".
5. Get notifications of birthdays for today.