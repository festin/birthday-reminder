var bDays
var storage = localStorage
var debugDegree = 0
var notifyDaysAhead = 5
var intervalMinutes = 60;
var columnName
var columnBirthdate
var now = new Date()

function show(person_name, interval) {
  //var time = /(..)(:..)/.exec(new Date());
  var notification = {title: '', body: ''}
  notification.title = person_name
  if (interval == 0) {
	notification.body = 'Today!'
  } else {
	notification.body = 'In ' + interval + ' day(s)'
  }
  new Notification(notification.title, {
    icon: 'cake128.png',
    body: notification.body
  });
}
function checkNow() {
	var today = {month: 0, day: 0}
	for (var i = notifyDaysAhead; i >= 0; i--) {
		var dayForCheck = new Date()
		dayForCheck.setDate(now.getDate() + i)
		var locale = "en-us"
		today.month = dayForCheck.toLocaleString(locale, { month: "short" })
		today.day = dayForCheck.getDate()
		todayBDays = bDays[today.month + ' ' + today.day]
		if (todayBDays != null) {
			for(var person of todayBDays) {
				show(person, i);
			}
		}
	}
}
function loadBDays() {
	if (storage.bDays != null) {
		bDays = JSON.parse(storage.bDays)
	} else {
		if (debugDegree > 0) console.log ("there is nothing to load from localStorage")
		bDays = {}
	}
}

columnName = storage.colName
columnBirthdate = storage.colBDate
chrome.extension.sendMessage({action: 'refreshUI', colName: columnName, colBDate: columnBirthdate})

if (window.Notification) {
  loadBDays();
  setInterval(checkNow, intervalMinutes * 60000);
  checkNow();
} else {
	alert("There is no notification support ?!?!?")
}
chrome.extension.onMessage.addListener(function(details) {
	var notification = {title: '', body: ''}
	if (details.type != null) {
		if (details.type == 'bDays') {
			bDays = details.content;
			storage.setItem("bDays", JSON.stringify(bDays))
			storage.setItem("colName", details.colName)
			storage.setItem("colBDate", details.colBDate)
			notification.title = 'Yeah!'
			notification.body = 'BDays received!'
		} else if (details.type == 'emptyBDays') {
			notification.title = 'No!'
			notification.body = 'Nothing here!'
		}
		new Notification(notification.title, {
			body: notification.body
		});
	}
});
